//import 'whatwg-fetch';

/**
 * Parses the JSON returned by a network request
 *
 * @param	 {object} response A response from a network request
 *
 * @return {object}					 The parsed JSON from the request
 */
function parseJSON(opts,response) {
	if(response.status === 401) return response;

	return response
		// map to a string so we can send non-JSON responses to raygun
		.text()
		.then(text => {
			try {
				return JSON.parse(text);
			} catch(e) {
				throw text;
			}
		})
		.then(data => ({
			...data,
			status: response.status,
		}))
		.catch((text) => {
			let err = new Error('Json parse failed');
			err.isJSONError = true;
			err.isStatusError = false;
			err.request = opts;
			err.response = text;
			throw err;
		});
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param	 {object} response	 A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(opts,response) {
	if (response.status >= 200 && response.status < 300 || response.status === 401) {
		return response;
	}

	const error = new Error(response.statusText);
	error.status = response.status;
	error.isStatusError = true;
	error.response = response;
	error.request = opts;
	throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param	 {string} url				The URL we want to request
 * @param	 {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}						The response data
 */
export default function request(url, options) {
	return fetch(url, options)
		.then(checkStatus.bind(undefined,{options,url}))
		.then(parseJSON.bind(undefined,{options,url}));
}

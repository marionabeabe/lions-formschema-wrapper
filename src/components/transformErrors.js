/**
 * Receive unformatted
 * error list and errors
 * are re-worded here
 */
import {get} from 'lodash';

export default function transformErrors(errors,data) {
	return errors.filter((error) => {
		// ignore type object errors

		if (error.name === 'type' && error.params.type !== 'integer') return false;
		// ignore required errors
		//console.log('DelegateEditor transformErrors', error);

		const isArrayField = error.property.includes("[");

		if((error.name === 'required') && !isArrayField) return false;
		if((error.name === "enum")) return false;
		if ([
			'.title',
			'.country',
			'.nationality',
			'.countryOfBirth',
			'.competition',
			'.companyData.companyActivity',
			'.arrivalDate',
			'.departureDate',
			'.hotel',
			'.budgetPerNight',
			'.accommodationCategory',
			'.numberOfOccupants',
			'.departureCountry',
			'.geographicalRegion.geographicalRegion_sub',
			'.jobDepartment',
			'.companyStage',
			'.companySector'
		].includes(error.property)) return false;
		

		if(isArrayField && error.name === 'required'){
			error.message = `${get(error, 'schema.title') || 'This field'} is required`;
		}

		if(error.name === "minItems"){

			const capitalize = (s) => {
				if (typeof s !== 'string') return ''
				return s.charAt(0).toUpperCase() + s.slice(1)
			  }

			const fieldName = error.property.split(".")[1];
			error.message = `${capitalize(fieldName) || 'This field'} requires at least one item`;
		}

		if(error.name === "type"){

			if(error.params.type === 'integer'){
				error.message = `Please enter a valid whole number`;
			}else{
				error.message = `Field is not in the correct format`;
			}
		}

		if(error.name === "enum"){
			error.message = `${get(error, 'schema.title') || 'This field'} is required`;
		}

		if (error.name === 'pattern' || error.property === '.emailAddress') {
			error.message = `${get(error, 'schema.title')|| 'This field'} is not in the correct format`;

			//format error shown is uneccessary if email field is empty.
			/*if(error.property === '.emailAddress' && (error.instance === undefined)){
				error.message = '';
			}*/

			//custom error message if pattern errors
			if(get(error, 'schema.patternErrorMessage')){
					error.message = get(error, 'schema.patternErrorMessage');
			}
		}

		if (error.name === 'format') {
			//dont check format for empty email fields
			if (error.params.format && error.params.format === 'email'){
				const fieldName = error.property.split(".")[1];
				if(fieldName && data && data[fieldName] !== undefined && data[fieldName].length === 0){
					return false;
				}else {
					error.message = `${get(error, 'schema.title')|| 'This field'} is not in the correct format`;
				}
			}
		}

		if (error.property === '.pressUploads') {
			error.message =  'A minimum of one item has to be uploaded';
		}

		if (error.property === '.stayPeriod') {
			error.message =  'Please select at least one option.';
		}

		return true;
	});
};

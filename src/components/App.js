import React from 'react';
import ReactFormValidation from '../../submodules/lions-react-form-validation';
import { Map, fromJS } from 'immutable';
import transformErrors from './transformErrors';
import './App.css';
import queryString from 'query-string';
import { isArray, pick } from 'lodash';


import {get, result} from 'lodash';
import request from '../utils/request';

const appRootHtml = (
  document.getElementById('ReactSchemaForm')
);

export default class formWrapper extends React.Component {

  // static defaultProps = {
  //   uploadTransform: () => {},
  // };
	constructor(props) {
		super(props);
		this.state = {
      schema:{},
      uiSchema:{},
      formData:{},
      loading:true,
		};
		this.onChange = this.onChange.bind(this);
	}


	componentDidMount(){

    //Comment this out for now, only used for DEV when pulling from API. Can be extended for proper use later.
    //this.fetchData();


    this.setState({
      schema:this.getSchema(),
      uiSchema:this.getUISchema(),
      formData:this.getFormData(),
      loading:false,
    })
    // console.log('approot',appRootHtml);
	}

  submitForm(data){


    console.log('submit data',data);
    //console.log('submitURL',submitURL);
    const submitURL = appRootHtml.getAttribute('submit-url');
    var form = document.createElement('form');
    form.setAttribute('method','POST');
    form.setAttribute('action',submitURL);
    
    var hiddenField = document.createElement('input');
    hiddenField.setAttribute('name','formData');
    hiddenField.setAttribute('type','hidden');
    hiddenField.setAttribute('value',`${JSON.stringify(data.formData)}`);
    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();

  }

  onChange(data){
    console.log('data',data);
  };

  onError(errs){
    console.log('errs',errs);
  }

  /**test data from api */
  fetchData(){
    let data = {}
    fetch("http://api-local.canneslions.com:8888/index.cfm/1-1/delegate/?apiKey=gsBxgVZS4g2VsMSps4fMzrp35KappqN5kF2AQUEg&registrationId=5AC31902-4490-EA11-B6C2-22000A0B3B4A&eventId=31")
    .then(res => res.json())
    .then((result) => {
      console.log('result',result);

      this.setState({
        loading:false,
        schema:fromJS(result.data.form.schema),
        uiSchema:fromJS(result.data.form.uiSchema),
        formData:fromJS(result.data.userData),
      })
    },
    (error) => {
      console.log('error',error)
    }
    );
  }


  getSchema(){

  /* Mock Data*/
  // const data = {
  //   "required": ["user","group", "passes","people"],
  //   "properties": {				
  //     "user": {
  //       "enumNames": ["one","two","three"],
  //       "type": "number",
  //       "enum": [1,2,3],
  //       "title": "User"
  //     },
  //     "people":{
  //       "title": "People select",
  //       "type": "string",
  //     },
  //     "mobilePhone":{
  //       "title": "Mobile",
  //       "type": "string",
  //       "pattern": "^$|^[+](?!( ))((\d| ){5,})",
  //     },
  //     "passes":{
  //       type: "array",
  //       minItems: 1,
  //       title: "Passes",
  //       items: {
  //         "type": "object",
  //         "required": ["registrationPassId","quantity"],
  //         "properties": {
  //           "registrationPassId": {
  //             "enumNames": ["one","two","three"],
  //             "type": "number",
  //             "enum": [1,2,3],
  //             "title": "Pass Name"
  //           },
  //           "quantity": {
  //             "type": "integer",
  //             "title": "Quantity",
  //             "min": 1,
  //             "default": 1
  //           }
  //         }
  //       },
  //     },
  //     "group": {
  //       "enumNames": ["one","two","three"],
  //       "type": "number",
  //       "enum": [1,2,3],
  //       "title": "Group"
  //     }
  //   },
  //   "type": "object"
  
  // };
  //  return fromJS(data);//JSON.stringify(data);


  //grab JSON string from root component
  const JSONString = appRootHtml.getAttribute('schema');
  const JSONParsed = JSON.parse(JSONString);

  JSONParsed.description = "Compulsory fields are labelled in <strong>bold</strong>";
  const JSONImmutable = fromJS(JSONParsed);
  return JSONImmutable;

}

  getUISchema(){
    /*Mock Data for Local*/
    // const data =  {
    //   "ui:toggles": [],
    //   "ui:order": ["image","user","passes","mobilePhone","group","people"],
    //   "user": {
    //     "ui:autofocus": false,
    //     "ui:disabled": false,
    //     "ui:placeholder": "Please choose...",
    //     "ui:widget": "select"
    //   },
    //   "people":{
    //     "ui:field": "dmsSelectAutoComplete",
    //     "ui:options":  {
    //       "serviceUrl": "/events/registrations/_autocomplete-users?persontype=user",
    //       "minChars": 4
    //     },
    //   },
    //   "mobilePhone":{
    //     "ui:placeholder":"MOB",
    //     "ui:widget":"phoneField",
    //     "ui:options": {
    //       "countryCallingCodes": [
    //         {
    //           "callingCode":"+93",
    //           "countryCode":"AF",
    //           "countryID": 1
    //         }
    //       ]
    //     }
    //   },
    //   "passes":{
    //     "ui:options":  {
    //       "orderable": false,
    //     },
    //     items: {
    //         "registrationPassId":{
    //           "ui:options": {
    //             "isMultiSelect": true
    //           },
    //         "ui:placeholder": "Please choose..."
    //         }
    //     },  
    //   },
    //   "group": {
    //     "ui:autofocus": false,
    //     "ui:disabled": false,
    //     "ui:placeholder": "Please choose..."
    //   }
    // }
    // return fromJS(data);//JSON.stringify(data);


    //grab JSON string from root component
    const JSONString = appRootHtml.getAttribute('schema-ui');
    const JSONParsed = JSON.parse(JSONString);
    const JSONImmutable = fromJS(JSONParsed);
    return JSONImmutable;

  }

  getFormData(){
    /*Mock Data for Local*/
    // const data = {
    //   "image": {
		// 		"clientFilename": "",
		// 		"filespinId": "",
		// 		"mimeType": "",
		// 		"paths": {}
		// 	}
    // };
    // return fromJS(data);//JSON.stringify(data);

    const JSONString = appRootHtml.getAttribute('form-data');
    if(JSONString === undefined || JSONString === null){
      return fromJS({});
    }
    const JSONParsed = JSON.parse(JSONString);
    const JSONImmutable = fromJS(JSONParsed);
    return JSONImmutable;
  
  }
  
  getUploadTransform () {


    //For Local Testing:
    // const currentQueries = {
    //   "eventId": "31",
    //   "userPersonID": "eb4962f6-a4b2-e811-99af-22000a4aa935"
    //   "registrationId":"B432E280-510D-EB11-B6C2-22000A0B3B4A",
    // }
    // const queries = queryString.stringify(currentQueries);
    //const uploadURL = `http://api-local.canneslions.com:8888/index.cfm/1-0/registration/upload?${queries}`;

    //Upload Transform Aysnc Function which calls WebApi to set newly updated filespin upload      
    this.uploadTransformFn = ({result, options, registrationUploadId}) => {
      console.log('result',result);
      console.log('options',options);

      let uploadURL = appRootHtml.getAttribute('upload-url');
      console.log('uploadURL',uploadURL);

      console.log('calling web api upload');
      if (!options.url.match(/filespin/)) return result;
      //const url = `${conf('api')}${conf('delegatePhotoURL')}?${queries}`;
      const url = uploadURL;
      console.log('url',url);
      const firstFile = get(result, ['files', 0]);
  
      if (!firstFile) return Promise.reject(new Error('No files uploaded'));
  
      let body = {
        clientFilename: firstFile.name,
        mimeType: firstFile.content_type,
        fileSpinId: firstFile.id,
        uploadType: options.uploadType,
      };
  
      if(registrationUploadId) {
        body.registrationUploadId = registrationUploadId;
      };
      const requestOptions = { };
  
        return request(url, {
          ...requestOptions,
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
          method: 'POST',
        })
        .then((result) => {
          console.log('resres',result);
         return Promise.resolve(result.data)
        });
    };


    

    return this.uploadTransformFn;
  }


	render() {


    if(this.state.loading){
      return(<div>Loading Form...</div>);
    }


    const cancelButton = appRootHtml.getAttribute('cancel-url');
    const submitLabel = appRootHtml.getAttribute('submit-label') ? appRootHtml.getAttribute('submit-label') : Submit;

		return (
      <div>
        <ReactFormValidation
          schema={this.state.schema}
          uiSchema={this.state.uiSchema}
          onChange={this.onChange.bind(this)}
          onSubmit={(data) => this.submitForm(data)}
          onValidateError={this.onError.bind(this)}
          transformErrors={transformErrors}
          formData={this.state.formData}
          uploadTransform={this.getUploadTransform()}
          noHtml5Validate={true}
        >
          <button type="submit" className="react-form-submit">{submitLabel}</button>
          {cancelButton && (
            <a href={cancelButton} className="react-form-cancel">Cancel</a>
          )}
        </ReactFormValidation>
      </div>
    );
	}
}











